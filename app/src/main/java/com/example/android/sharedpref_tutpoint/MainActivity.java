package com.example.android.sharedpref_tutpoint;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText ed1,ed2,ed3;
    Button b1;

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String Name = "nameKey";
    public static final String Phone = "phoneKey";
    public static final String Email = "emailKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ed1=findViewById(R.id.uId);
        ed2=findViewById(R.id.uPhone);
        ed3=findViewById(R.id.uEmail);

        b1=findViewById(R.id.btnSave);

        final SharedPreferences sharedPreferences=getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_id=ed1.getText().toString();
                String user_phone=ed2.getText().toString();
                String user_email=ed3.getText().toString();

                SharedPreferences.Editor editor=sharedPreferences.edit();

                editor.putString(Name,user_id);

                editor.putString(Phone,user_phone);
                editor.putString(Email,user_email);
                editor.commit();
                Toast.makeText(MainActivity.this,"Thanks"+sharedPreferences.getAll().toString(),Toast.LENGTH_LONG).show();
            }
        });
    }
}
